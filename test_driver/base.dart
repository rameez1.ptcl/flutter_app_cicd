// Imports the Flutter Driver API.
import 'dart:io';
import 'package:path/path.dart';
import 'package:flutter_driver/flutter_driver.dart';
import 'package:test/test.dart';

void main() {


  group('login to the app', ()
  {
    // First, define the Finders and use them to locate widgets from the
    // test suite. Note: the Strings provided to the `byValueKey` method must
    // be the same as the Strings we used for the Keys in step 1.
    FlutterDriver driver;

    // Connect to the Flutter driver before running any tests.
    setUpAll(() async {
      // TestWidgetsFlutterBinding.ensureInitialized();

      final envVars = Platform.environment;
      final adbPath = join(
        envVars['ANDROID_SDK_ROOT'] ?? envVars['ANDROID_HOME'],
        'platform-tools',
        Platform.isWindows ? 'adb.exe' : 'adb',
      );
      await Process.run(adbPath, [
        'shell',
        'pm',
        'grant',
        'com.mobidevlabs.ande', // replace with your app id
        'android.permission.CAMERA'
      ]);
      driver = await FlutterDriver.connect();
    });

    // Close the connection to the driver after the tests have completed.
    tearDownAll(() async {
      if (driver != null) {
        driver.close();
        print("testing done");
      }
    });

    test('press on the counter', () async {
      await Future.delayed(const Duration(seconds: 3), () {
        print('welcome to the app');
      });
      for(int i=0 ; i<5 ; i++){
      await driver.tap(find.byValueKey("press"));
      await Future.delayed(const Duration(seconds: 3),);

      } // first commit
    });
  });
}
